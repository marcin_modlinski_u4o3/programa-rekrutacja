# Programa — rekrutacja #
## Spis treści
* [Opis](#opis)
* [Technologie](#technologie)
* [Zależności](#zależności)
* [Instalacja](#instalacja)

## Opis
Zbiór sześciu rozwiązanych zadań rekrutacyjnych zebranych pod postacią jednej aplikacji.
Możliwe jest jej uruchomienie z wykorzystaniem Docker (niżej).

## Technologie
* PHP >= 7.2
* Composer

## Zależności
* twig/twig >= 2.0
* twbs/bootstrap >= 4.1
* components/jquery >= 3.3
* doctrine/orm >= 2.6,
* phpunit/phpunit >= 6.5

## Instalacja
1. Korzystając z Dockera
    * Wykorzystując Dockera i jego polecenie `docker-compose up` utworzyć kontener pozwalający uruchomić aplikację.
    * Aplikacja będzie dostępna pod adresem: `http:\\localhost`
    * Baza danych będzie dostępna pod adresem: `http:\\localhost:8080` (u:user, p:userpass, db:mydb). 
2. Z pominięciem Dockera
    * Przekopiować zawartość katalogu `code` do lokalizacji umożliwiającej uruchomienie aplikacji z użyciem serwera nginx/Apache i PHP.
    Zrzut bazy danych znajduje się w lokalizacji: `/docker/php7-fpm/mydb.sql`.
3. Instalacja wymaganych bibliotek (zależności) z wykorzystaniem Composer.
4. Uruchomienie aplikacji odbywa się przy użyciu pliku `./code/index.php`.

![Wygląd aplikacji](screen.png).

## Autor
Marcin Modliński
