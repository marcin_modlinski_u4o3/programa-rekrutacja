-- Adminer 4.7.0 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `category` (`id`, `name`) VALUES
(1,	'Pieczywo'),
(2,	'Rogale'),
(3,	'Kasze'),
(4,	'Owoce');

DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_name` varchar(255) NOT NULL,
  `menu_parent` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

INSERT INTO `menu` (`id`, `menu_name`, `menu_parent`) VALUES
(1,	'Kategoria główna',	0),
(2,	'Podkategoria 1',	1),
(3,	'Produkt A',	2),
(4,	'Produkt B',	2),
(5,	'Podkategoria 2',	1),
(6,	'Produkt C',	5),
(7,	'Produkt D',	5);

DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `price` decimal(8,2) NOT NULL,
  `availability` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `product` (`id`, `name`, `description`, `price`, `availability`) VALUES
(1,	'Rogal razowy',	NULL,	0.85,	1),
(2,	'Chleb razowy',	NULL,	2.45,	1),
(3,	'Kasza razowa',	NULL,	1.60,	1),
(4,	'Rogal z makiem',	NULL,	0.80,	1),
(5,	'Arbuz',	NULL,	3.20,	0),
(6,	'Banan',	NULL,	1.20,	0);

DROP TABLE IF EXISTS `product_category`;
CREATE TABLE `product_category` (
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`,`category_id`),
  KEY `IDX_CDFC73564584665A` (`product_id`),
  KEY `IDX_CDFC735612469DE2` (`category_id`),
  CONSTRAINT `FK_CDFC735612469DE2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`),
  CONSTRAINT `FK_CDFC73564584665A` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `product_category` (`product_id`, `category_id`) VALUES
(1,	1),
(1,	2),
(2,	1),
(3,	3),
(4,	1),
(4,	2),
(5,	4),
(6,	4);

-- 2018-12-16 21:46:46
