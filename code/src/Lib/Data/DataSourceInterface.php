<?php

namespace Lib\Data;

/**
 * Interface DataSourceInterface
 * @package Lib\Data
 * @codeCoverageIgnore
 */
interface DataSourceInterface
{
    public function __construct($source);
    public function getContext();
}