<?php

namespace Lib\Data;

/**
 * Class DataProvider
 * @package Lib\Data
 */
class DataProvider implements DataProviderInterface
{
    /** @var DataSourceInterface */
    private $source;

    /**
     * DataProvider constructor.
     * @param DataSourceInterface $source
     */
    public function __construct(DataSourceInterface $source)
    {
        $this->source = $source;
    }

    /**
     * @return string|mixed
     */
    public function getProvider()
    {
        return $this->source->getContext();
    }
}