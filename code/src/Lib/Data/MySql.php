<?php

namespace Lib\Data;


/**
 * Class MySql
 * @package Lib\Data
 */
class MySql implements DataSourceInterface
{
    /** @var MySqlConnection */
    private $source;

    /**
     * MySql constructor.
     * @param $source
     */
    public function __construct($source)
    {
        $this->source = $source;
    }

    /**
     * @return false|string
     */
    public function getContext()
    {
        return $this->source->getConnection();
    }
}