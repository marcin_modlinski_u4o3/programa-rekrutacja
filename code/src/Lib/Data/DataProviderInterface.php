<?php

namespace Lib\Data;

/**
 * Interface DataProvider
 * @package Lib\Data
 * @codeCoverageIgnore 
 */
interface DataProviderInterface
{
    public function __construct(DataSourceInterface $source);
    public function getProvider();
}