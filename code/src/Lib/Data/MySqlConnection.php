<?php

namespace Lib\Data;

use PDO;
use PDOException;

/**
 * Class MySql
 * @package Lib\Data
 */
class MySqlConnection
{
    /** @var MySql */
    private static $instance = null;
    private $connection;
    // host=db ← Docker → docker-compose.yml → serwis:db
    private const ACCESS = "mysql:host=db;port=3306;dbname=mydb;charset=utf8";
    private const USER   = "user";
    private const PASS   = "userpass";

    /**
     * MySqlConnection constructor.
     */
    private function __construct()
    {
        try{
            $this->connection = new PDO(self::ACCESS, self::USER, self::PASS);
        }catch(PDOException $e){
            echo $e->getmessage();
        }
    }

    /**
     * @return MySql|MySqlConnection
     */
    public static function getInstance()
    {
        if(!self::$instance)
        {
            self::$instance = new MySqlConnection();
        }

        return self::$instance;
    }

    /**
     * @return \PDO
     */
    public function getConnection()
    {
        return $this->connection;
    }
}