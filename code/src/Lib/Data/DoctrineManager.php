<?php

namespace Lib\Data;

use Doctrine\ORM\{EntityManager, Tools\Setup};

/**
 * Class DoctrineManager
 * @package Lib\Data
 */
class DoctrineManager implements DataSourceInterface
{

    /**
     * DoctrineManager constructor.
     * @param null $source
     */
    public function __construct($source = null)
    {
    }

    /**
     * @return EntityManager
     * @throws \Doctrine\ORM\ORMException
     */
    public function getContext()
    {
        $config     = Setup::createAnnotationMetadataConfiguration(
            [__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..'],
            true
        );
        $connParams = [
            'dbname'   => 'mydb',
            'user'     => 'user',
            'password' => 'userpass',
            'host'     => 'db',
            'driver'   => 'pdo_mysql',
        ];

        return EntityManager::create($connParams, $config);
    }
}