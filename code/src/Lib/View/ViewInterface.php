<?php

namespace Lib\View;

/**
 * Interface ViewInterface
 * @package Lib\View
 * @codeCoverageIgnore
 */
interface ViewInterface
{
    public static function render(string $name, array $context = []): void;
}