<?php

namespace Lib\View;

use Twig_Environment;
use Twig_Loader_Filesystem;

/**
 * Class View
 * @package Lib\View
 * @codeCoverageIgnore
 */
class View implements ViewInterface
{
    /**
     * @param string $name
     * @param array $context
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public static function render(string $name, array $context = []): void
    {
        $loader = new Twig_Loader_Filesystem(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR);
        $twig   = new Twig_Environment($loader);

        echo $twig->render($name, $context);
    }
}