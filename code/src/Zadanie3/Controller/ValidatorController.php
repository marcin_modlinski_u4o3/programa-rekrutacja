<?php

namespace Zadanie3\Controller;

use Lib\View\{View, ViewInterface};

/**
 * Class CvController
 * @package Zadanie3\Controller
 */
class ValidatorController
{
    public function index(): void
    {
        $this->renderView(new View(), 'Zadanie3/View/index.html.twig', []);
    }

    /**
     * @param ViewInterface $view
     * @param string $name
     * @param array $context
     */
    private function renderView(ViewInterface $view, string $name, array $context = []): void
    {
        $view::render($name, $context);
    }
}