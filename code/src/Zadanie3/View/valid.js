const form = document.getElementById('person');
form.addEventListener('submit', event => {
    error = false;
    validate('name').isRequired();
    validate('surname').isRequired();
    validate('age').isRequired().isInRange(18,99);
    validate('sex').isRequired();
    validate('agree').isChecked();
    if (error) {
        event.preventDefault();
    }
});
form.element = function(name) {
    return form.elements.namedItem(name);
};

let validate = function(name) {
    let element;
    let message;
    let obj   = {
        getElement(name) {
            if (element) {
                return element
            }
            return form.element(name);
        },
        addErrorMessage() {
            if (null == element.nextElementSibling) {
                let spanTag = document.createElement('span');
                spanTag.innerHTML = message;
                spanTag.classList.add('error');
                element.classList.add('error');
                element.parentNode.appendChild(spanTag);
            }
            return element;
        },
        removeErrorMessage() {
            element.classList.remove('error');
            if (null !== element.nextElementSibling) {
                element.nextElementSibling.remove();
            }
            return obj;
        },
        isRequired() {
            if (element.value.length < 1) {
                message = 'To pole nie może być puste!';
                this.addErrorMessage(element);
                error = true;
            } else {
                this.removeErrorMessage(element)
            }
            return obj;
        },
        isInRange(min, max) {
            if (min > element.value || element.value > max) {
                message = 'Wartość pola musi się zawierać między ' + min + ', a ' + max;
                this.addErrorMessage(element);
                error = true;
            } else {
                this.removeErrorMessage(element)
            }
            return obj;
        },
        isChecked() {
            if (!element.checked) {
                message = 'To pole musi być zaznaczone!';
                this.addErrorMessage(element);
                error = true;
            } else {
                this.removeErrorMessage(element)
            }
            return obj;
        }
    };

    element = obj.getElement(name);

    return obj;
};