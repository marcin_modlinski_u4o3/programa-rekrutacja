<?php

namespace Zadanie1\Controller;

use Lib\View\{View, ViewInterface};

/**
 * Class NumberController
 * @package Zadanie1\Controller
 */
class NumberController
{
    /**
     */
    public function index(): void
    {
        $number = new \Zadanie1\Model\Number();
        $number->addNumber(1)
               ->addNumber(3)
               ->addNumber(5)
               ->addNumber(2)
               ->addNumber(4)
               ->addNumber(6)
               ->addNumber(11)
               ->addNumber(13)
               ->addNumber(15)
               ->addNumber(17);

        $print  = $number->printNumbers();
        $report = $number->getReport();

        $this->renderView(new View(), 'Zadanie1/View/index.html.twig', [
            'print'  => $print,
            'report' => $report
        ]);
    }

    /**
     * @param ViewInterface $view
     * @param string $name
     * @param array $context
     */
    private function renderView(ViewInterface $view, string $name, array $context = []): void
    {
        $view::render($name, $context);
    }
}