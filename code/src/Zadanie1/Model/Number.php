<?php

namespace Zadanie1\Model;

/**
 * Class Number
 * @package Zadanie1\Model
 */
class Number implements \Countable
{
    /** @var array */
    private $numbers = [];

    /**
     * @inheritdoc
     */
    public function count(): int
    {
        return count($this->numbers);
    }

    /**
     * @return array
     */
    public function getNumbers(): array
    {
        return $this->numbers;
    }

    /**
     * @param int $number
     * @return Number
     */
    public function addNumber(int $number): self
    {
        if (0 > $number) {
            throw new \InvalidArgumentException($number . ' nie jest liczbą naturalną');
        }
        $this->numbers[] = $number;

        return $this;
    }

    /**
     * @return bool
     */
    private function calculateReport()
    {
        $evenNumbers = $oddNumbers = 0;
        foreach ($this->getNumbers() as $number) {
            (0 == $number % 2) ? $evenNumbers++ : $oddNumbers++;
        }

        $condition = false;
        if (3 * $evenNumbers > 2 * $oddNumbers) {
            $condition = true;
        }

        return $condition;
    }
    
    /**
     * @return string
     */
    public function getReport(): string
    {
        if (!$this->count()) {
            return 'Brak liczb do wygenerowania raportu!';
        }
        $condition = ($this->calculateReport()) ? 'jest' : 'nie jest';

        return 'Trzykrotność liczb parzystych <strong>' . $condition . '</strong> większa niż dwukrotność nieparzystych';
    }

    /**
     * @return string
     */
    public function printNumbers(): string
    {
        if ($this->count()) {
            $numbers = implode(", ", $this->getNumbers());
        }

        return $numbers ?? 'brak liczb';
    }
}