<?php

namespace Zadanie6\Model;

use Doctrine\Common\Collections\{ArrayCollection, Collection};

/**
 * Class Product
 * @package Zadanie6\Model
 * @Entity @Table(name="product")
 */
class Product
{
    /**
     * @var int
     *
     * @Id @Column(name="id", type="integer") @GeneratedValue
     */
    private $id;

    /**
     * @var string
     *
     * @Column(name="name", type="string")
     */
    private $name;

    /**
     * @var string
     *
     * @Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var float
     *
     * @Column(name="price", type="decimal", precision=8, scale=2)
     */
    private $price;

    /**
     * @var bool
     *
     * @Column(name="availability", type="boolean")
     */
    private $availability;

    /**
     * @var Collection|Category[]
     *
     * @ManyToMany(targetEntity="Zadanie6\Model\Category", inversedBy="products", )
     * @JoinTable(
     *  name="product_category",
     *  joinColumns={
     *      @JoinColumn(name="product_id", referencedColumnName="id")
     *  },
     *  inverseJoinColumns={
     *      @JoinColumn(name="category_id", referencedColumnName="id")
     *  }
     * )
     */
    private $categories;

    /**
     * Product constructor.
     */
    public function __construct()
    {
        $this->categories = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice(float $price): void
    {
        $this->price = $price;
    }

    /**
     * @return bool
     */
    public function isAvailability(): bool
    {
        return $this->availability;
    }

    /**
     * @param bool $availability
     */
    public function setAvailability(bool $availability): void
    {
        $this->availability = $availability;
    }

    /**
     * @return Collection
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    /**
     * @param Category $category
     */
    public function addCategory(Category $category): void
    {
        if ($this->categories->contains($category)) {
            return;
        }
        $this->categories->add($category);
        $category->addProduct($this);
    }

    /**
     * @param Category $category
     */
    public function removeCategory(Category $category): void
    {
        if (!$this->categories->contains($category)) {
            return;
        }
        $this->categories->removeElement($category);
        $category->removeProduct($this);
    }
}