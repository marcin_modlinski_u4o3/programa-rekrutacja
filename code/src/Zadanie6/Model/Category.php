<?php

namespace Zadanie6\Model;

use Doctrine\Common\Collections\{ArrayCollection, Collection};

/**
 * Class Category
 * @package Zadanie6\Model
 * @Entity @Table(name="category")
 */
class Category
{
    /**
     * @var int
     *
     * @Id @Column(name="id", type="integer") @GeneratedValue
     */
    private $id;

    /**
     * @var string
     *
     * @Column(name="name", type="string")
     */
    private $name;

    /**
     * @var Collection|Product[]
     *
     * @ManyToMany(targetEntity="Zadanie6\Model\Product", mappedBy="categories")
     * @JoinTable(name="product_category")
     */
    private $products;

    /**
     * Category constructor.
     */
    public function __construct()
    {
        $this->products = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return Collection
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    /**
     * @param Product $product
     */
    public function addProduct(Product $product): void
    {
        if ($this->products->contains($product)) {
            return;
        }
        $this->products->add($product);
        $product->addCategory($this);
    }
    /**
     * @param Product $product
     */
    public function removeProduct(Product $product): void
    {
        if (!$this->products->contains($product)) {
            return;
        }
        $this->products->removeElement($product);
        $product->removeCategory($this);
    }
}