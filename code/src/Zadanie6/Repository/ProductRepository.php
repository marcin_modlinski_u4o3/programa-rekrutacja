<?php

namespace Zadanie6\Repository;

use Zadanie6\Model\Product;

/**
 * Class ProductRepository
 * @package Zadanie6\Repository
 */
class ProductRepository extends DoctrineRepositoryAbstract
{
    /**
     * @return mixed
     */
    public function getAllProducts(): array
    {
        $productRepository = $this->entityManager->getRepository('Zadanie6\Model\Product');
        $products = $productRepository->findAll();

        return $products;
    }

    /**
     *
     * @return int
     */
    public function countAvailableProducts(): int
    {
        $productRepository = $this->entityManager->getRepository('Zadanie6\Model\Product');
        $products = $productRepository->count(['availability' => true]);

        return $products;
    }

    /**
     * @return array|Product[]
     */
    public function getNotAvailableProducts(): array
    {
        $productRepository = $this->entityManager->getRepository('Zadanie6\Model\Product');
        $products = $productRepository->findBy(['availability' => false]);

        return $products;
    }

    /**
     * @param string $string
     * @return array
     */
    public function findByDescription(string $string): array
    {
        $productRepository = $this->entityManager->getRepository('Zadanie6\Model\Product');
        $products = $productRepository->createQueryBuilder('p')
            ->where('p.name LIKE :string')
            ->setParameter('string', '%'.$string.'%')
            ->getQuery()
            ->getResult();

        return $products;
    }

    /**
     * @param int $categoryId
     * @return int
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function countProductsInTheCategory(int $categoryId): int
    {
        $productRepository = $this->entityManager->getRepository('Zadanie6\Model\Product');
        $products = $productRepository->createQueryBuilder('p')
            ->select('COUNT(p.id)')
            ->leftJoin('p.categories', 'c')
            ->where('c.id = :id')
            ->andWhere('p.availability = :av')
            ->setParameter('id', $categoryId)
            ->setParameter('av', true)
            ->getQuery()
            ->getSingleScalarResult();

        return $products;
    }

    /**
     * @param int $categoryId
     * @param string|null $sort
     * @return array
     */
    public function getSortableProductInTheCategory(int $categoryId, ?string $sort = null): array
    {
        $tempSort   = (in_array(strtolower($sort),['asc', 'desc'])) ? $sort : null;
        $productRepository = $this->entityManager->getRepository('Zadanie6\Model\Product');
        $products = $productRepository->createQueryBuilder('p')
            ->leftJoin('p.categories', 'c')
            ->where('c.id = :id')
            ->orderBy('p.name', $tempSort)
            ->setParameter('id', $categoryId)
            ->getQuery()
            ->getResult();

        return $products;
    }
}