<?php

namespace Zadanie6\Repository;

use Doctrine\ORM\EntityManager;
use Lib\Data\{DataProvider, DoctrineManager};

/**
 * Class DoctrineReposotoryAbstract
 * @package Zadanie6\Repository
 */
abstract class DoctrineRepositoryAbstract
{
    /** @var EntityManager */
    protected $entityManager;

    /**
     * MenuRepository constructor.
     */
    public function __construct()
    {
        $dp = new DataProvider(new DoctrineManager());
        $this->entityManager = $dp->getProvider();
    }
}