<?php

namespace Zadanie6\Controller;

use Lib\View\{View, ViewInterface};
use Zadanie6\Repository\ProductRepository;

/**
 * Class ProductController
 * @package Zadanie6\Controller
 */
class ProductController
{
    public function index(): void
    {
        $repo = new ProductRepository();

        $this->renderView(new View(), 'Zadanie6/View/index.html.twig', [
            'allProducts'            => $repo->getAllProducts(),
            'countAvailableProducts' => $repo->countAvailableProducts(),
            'notAvailableProducts'   => $repo->getNotAvailableProducts(),
            'findByDescription'      => $repo->findByDescription('razowy'),
            'countProductsInTheCategoryId'     => $repo->countProductsInTheCategory(1),
            'sortableProductInTheCategoryAsc'  => $repo->getSortableProductInTheCategory(1, 'asc'),
            'sortableProductInTheCategoryDesc' => $repo->getSortableProductInTheCategory(4, 'desc'),
        ]);
    }

    /**
     * @param ViewInterface $view
     * @param string $name
     * @param array $context
     */
    private function renderView(ViewInterface $view, string $name, array $context = []): void
    {
        $view::render($name, $context);
    }
}