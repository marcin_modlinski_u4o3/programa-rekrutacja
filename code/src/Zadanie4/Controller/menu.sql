CREATE TABLE IF NOT EXISTS menu
(
  id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  menu_name VARCHAR(255) NOT NULL COLLATE utf8_general_ci,
  menu_parent INT(10) UNSIGNED NOT NULL COLLATE utf8_general_ci,
  PRIMARY KEY (id),
  INDEX menu_name (menu_name),
  INDEX menu_parent (menu_parent)
)
CHARACTER SET utf8 COLLATE utf8_general_ci
ENGINE=InnoDB;

INSERT menu (id, menu_name, menu_parent) VALUES
(1,'Kategoria główna',0),
(2,'Podkategoria 1',1),
(3,'Produkt A',2),
(4,'Produkt B',2),
(5,'Podkategoria 2',1),
(6,'Produkt C',5),
(7,'Produkt D',5);