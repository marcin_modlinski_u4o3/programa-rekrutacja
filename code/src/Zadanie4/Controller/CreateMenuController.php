<?php

namespace Zadanie4\Controller;

use Lib\View\{View, ViewInterface};

/**
 * Class CreateMenu
 * @package Zadanie4\Controller
 */
class CreateMenuController
{
    public function index(): void
    {
        $this->renderView(new View(), 'Zadanie4/View/index.html.twig', []);
    }

    /**
     * @param ViewInterface $view
     * @param string $name
     * @param array $context
     */
    private function renderView(ViewInterface $view, string $name, array $context = []): void
    {
        $view::render($name, $context);
    }
}
