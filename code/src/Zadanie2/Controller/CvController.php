<?php

namespace Zadanie2\Controller;

use Lib\View\{View, ViewInterface};

/**
 * Class CvController
 * @package Zadanie2\Controller
 */
class CvController
{
    public function index(): void
    {
        $this->renderView(new View(), 'Zadanie2/View/index.html.twig', []);
    }

    /**
     * @param ViewInterface $view
     * @param string $name
     * @param array $context
     */
    private function renderView(ViewInterface $view, string $name, array $context = []): void
    {
        $view::render($name, $context);
    }
}