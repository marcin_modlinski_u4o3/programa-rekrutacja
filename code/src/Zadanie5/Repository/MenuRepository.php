<?php

namespace Zadanie5\Repository;

use Lib\Data\{DataProvider, MySql, MySqlConnection};
use PDO;

/**
 * Class MenuRepository
 * @package Zadanie5\Repository
 */
class MenuRepository
{
    /** @var PDO  */
    private $db;

    /**
     * MenuRepository constructor.
     */
    public function __construct()
    {
        $dp = new DataProvider(new MySql(MySqlConnection::getInstance()));
        $this->db = $dp->getProvider();
    }

    /**
     * @return array
     */
    public function getMenu(): array
    {
        $menu_db = $this->db->query(
            "
                  SELECT id, menu_name, menu_parent 
                  FROM menu
                  ",
            PDO::FETCH_ASSOC
        );

        return $menu_db->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @param int $parent
     * @return array
     */
    public function getMenuRecursion(int $parent = 0): array
    {
        $menu_query = $this->db->prepare(
            "
                      SELECT id, menu_name, menu_parent 
                      FROM menu
                      WHERE menu_parent = :parent
                    "
        );
        $menu_query->execute([':parent' => $parent]);

        return $menu_query->fetchAll(PDO::FETCH_ASSOC);
    }
}