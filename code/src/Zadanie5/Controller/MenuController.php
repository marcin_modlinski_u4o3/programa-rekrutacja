<?php

namespace Zadanie5\Controller;

use Lib\View\{View, ViewInterface};
use Zadanie5\Repository\MenuRepository;

/**
 * Class CreateMenu
 * @package Zadanie4\Controller
 */
class MenuController
{
    public function index(): void
    {
        $menu = $this->prepareMenu();
        $this->renderView(new View(), 'Zadanie5/View/index.html.twig', ['menu' => $menu]);
    }

    /**
     * @return string
     */
    private function prepareMenu(): string
    {
        $repo = new MenuRepository();
        $menu_by_parent = [];
        foreach ($repo->getMenu() as $item) {
            $menu_by_parent[$item['menu_parent']][] = $item;
        }
        return $this->generateMenu($menu_by_parent);
        //return $this->generateMenuRecursion($repo);
    }

    /**
     * Metoda ta generuje rekurencyjnie menu, do którego dane zostały pobrane jeden raz.
     * Dla uproszczenia strukturę hierarchii ul>li utworzono w tej metodzie.
     *
     * @param array $menu
     * @param int $parent
     * @return string
     */
    private function generateMenu(array $menu, int $parent = 0): string
    {
        if (isset($menu[$parent])) {
            $menu_string = '<ul>';
            foreach ($menu[$parent] as $menu_item) {
                $menu_string .= '<li>';
                $menu_string .= $menu_item['menu_name'];
                $menu_string .= $this->generateMenu($menu, $menu_item['id']);
                $menu_string .= '</li>';
            }
            $menu_string .= '</ul>';
        }

        return $menu_string ?? '';
    }

    /**
     * Metoda ta generuje rekurencyjnie menu, do którego dla każdej pozycji odpytywana jest baza danych.
     *
     * @param MenuRepository $repo
     * @param int $parent
     * @return string
     */
    private function generateMenuRecursion(MenuRepository $repo, int $parent = 0): string
    {
        $menu = $repo->getMenuRecursion($parent);
        if (!empty($menu)) {
            $menu_string = '<ul>';
            foreach ($menu as $menu_item) {
                $menu_string .= '<li>';
                $menu_string .= $menu_item['menu_name'];
                $menu_string .= $this->generateMenuRecursion($repo, $menu_item['id']);
                $menu_string .= '</li>';
            }
            $menu_string .= '</ul>';
        }

        return $menu_string ?? '';
    }

    /**
     * @param ViewInterface $view
     * @param string $name
     * @param array $context
     */
    private function renderView(ViewInterface $view, string $name, array $context = []): void
    {
        $view::render($name, $context);
    }
}
