<?php

namespace Tests\Zadanie1A\Model;

use PHPUnit\Framework\TestCase;
use Zadanie1A\Model\Number;

/**
 * Class NumberTest
 * @package units\Zadanie1\Model
 */
class NumberTest extends TestCase
{
    /** @var Number */
    private $number;

    protected function setUp()
    {
        $this->number = new Number();
    }

    public function testShouldReturnEmptyArray()
    {
        // Then
        $this->assertCount(0, $this->number);
        $this->assertSame('brak liczb', $this->number->printNumbers());
    }

    public function testShouldReturnOneCount()
    {
        // Given
        $this->number->addNumber(1);
        // Then
        $this->assertCount(1, $this->number);
        $this->assertSame('1', $this->number->printNumbers());
    }

    public function testShouldReturnTwoCount()
    {
        // Given
        $this->number->addNumber(2)->addNumber(3);
        // Then
        $this->assertCount(2, $this->number);
        $this->assertSame('2, 3', $this->number->printNumbers());
    }

    public function testShouldThrowExceptionWhenTheNumberIsNotInteger()
    {
        // Expect
        $this->expectException(\InvalidArgumentException::class);
        // Given
        $this->number->addNumber(-1);
    }

    public function testShouldGetFalseReport()
    {
        // Given
        $this->number->addNumber(2)->addNumber(4)->addNumber(1)->addNumber(3)->addNumber(5);
        // Then
        $this->assertSame(
            'Trzykrotność liczb parzystych <strong>nie jest</strong> większa niż dwukrotność nieparzystych',
            $this->number->getReport());
    }

    public function testShouldGetTrueReport()
    {
        // Given
        $this->number->addNumber(2)->addNumber(4)->addNumber(1)->addNumber(3);
        // Then
        $this->assertSame(
            'Trzykrotność liczb parzystych <strong>jest</strong> większa niż dwukrotność nieparzystych',
            $this->number->getReport());
    }
}