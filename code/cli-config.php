<?php

use Lib\Data\DoctrineManager;

require_once "index.php";

$dm = new DoctrineManager();
return \Doctrine\ORM\Tools\Console\ConsoleRunner::createHelperSet($dm->getContext());

