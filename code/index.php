<?php
    declare(strict_types = 1);

    if (!file_exists('vendor' . DIRECTORY_SEPARATOR . 'autoload.php')) {
        die('<h1>Proszę zainstalować wymagane biblioteki wykorzystując Composer!</h1>');
    }

    require_once 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';

    switch ($_GET['page']) {
        case 'zadanie1':
            $menu = new \Zadanie1\Controller\NumberController();
            break;
        case 'zadanie2':
            $menu = new \Zadanie2\Controller\CvController();
            break;
        case 'zadanie3':
            $menu = new \Zadanie3\Controller\ValidatorController();
            break;
        case 'zadanie4':
            $menu = new \Zadanie4\Controller\CreateMenuController();
            break;
        case 'zadanie5':
            $menu = new \Zadanie5\Controller\MenuController();
            break;
        case 'zadanie6':
            $menu = new \Zadanie6\Controller\ProductController();
            break;
        default:
            $menu = new \Zadanie1\Controller\NumberController();
    }
    $menu->index();

